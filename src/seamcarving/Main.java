package seamcarving;

import java.time.Instant;
import java.util.Scanner;
import java.util.function.Consumer;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    static SeamCarver seamCarver;
    static boolean shouldRepeat;
    static Consumer<SeamCarver> way;
    static int input;

    public static void main(String[] args) {
        System.out.println("Are you decreasing the picture's (h)eight or (w)idth?");
        System.out.println("Choose one:");
        do {
            shouldRepeat = false;
            String inWay = scanner.nextLine();
            try {
                way = validateWay(inWay);
            } catch (IllegalArgumentException e) {
                shouldRepeat = true;
                System.out.println("ERROR: " + e.getMessage());
            }
        } while (shouldRepeat);
        System.out.println("\nHow many threads to run? The least number of threads is 1:");
        int threadN = 1;
        do {
            shouldRepeat = false;
            String inThreads = scanner.nextLine();
            try {
                threadN = validateThreads(inThreads);
            } catch (IllegalArgumentException e) {
                shouldRepeat = true;
                System.out.println("ERROR: " + e.getMessage());
            }
        } while (shouldRepeat);
        System.out.println("\nEnter the image's path to resize:");
        do {
            shouldRepeat = false;
            String inImagePath = scanner.nextLine();
            try {
                Picture inputImg = new Picture(inImagePath);
                seamCarver = new SeamCarver(inputImg, threadN);
            } catch (IllegalArgumentException e) {
                shouldRepeat = true;
                System.out.println("ERROR: " + e.getMessage());
            }
        } while (shouldRepeat);

        way.accept(seamCarver);
    }

    public static Consumer<SeamCarver> validateWay(String inWay) throws IllegalArgumentException {
        if (inWay.equalsIgnoreCase("h")) {
            return Main::resizeHeight;
        }
        if (inWay.equalsIgnoreCase("w")) {
            return Main::resizeWidth;
        }

        throw new IllegalArgumentException("Invalid argument, choose either (h)eight or (w)idth");
    }

    public static int validateThreads(String inThreads) throws IllegalArgumentException {
        int number = Integer.parseInt(inThreads);
        if (number > 0) {
            return number;
        }

        throw new IllegalArgumentException("Invalid argument, input at least 1 as a number of threads");
    }

    public static void resizeWidth(SeamCarver seamCarver) {
        System.out.println("\nEnter new width:");
        do {
            shouldRepeat = false;
            try {
                input = Integer.parseInt(scanner.nextLine());
            } catch (IllegalArgumentException e) {
                shouldRepeat = true;
                System.out.println("ERROR: " + e.getMessage());
            }
        } while (shouldRepeat);

        try {
            System.out.println(Instant.now());
            Picture pic = seamCarver.resizeTo("width", input);
            System.out.println(Instant.now());
            pic.show();
        } catch (InterruptedException e) {
            System.out.println("ERROR: " + e.getMessage());
        }
    }

    public static void resizeHeight(SeamCarver seamCarver) {
        System.out.println("\nEnter new height:");
        int input = Integer.parseInt(scanner.nextLine());
        try {
            System.out.println(Instant.now());
            Picture pic = seamCarver.resizeTo("height", input);
            System.out.println(Instant.now());
            pic.show();
        } catch (InterruptedException e) {
            System.out.println("ERROR: " + e.getMessage());
        }
    }
}
