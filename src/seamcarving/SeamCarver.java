package seamcarving;

import java.awt.Color;
import java.lang.IllegalArgumentException;
import java.lang.IndexOutOfBoundsException;
import java.lang.Math;
import java.util.HashMap;
import java.util.function.*;

public class SeamCarver {
    private Picture pic;
    private int width;
    private int height;
    private int threadN;

    public SeamCarver(Picture picture, int threadN) {
        pic = new Picture(picture);
        width = pic.width();
        height = pic.height();
        this.threadN = threadN;
    }

    public Picture getPicture() {
        return pic;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public double energy(int x, int y) throws IndexOutOfBoundsException {
        if (x < 0 || x >= getWidth() || y < 0 || y >= getHeight()) {
            throw new IndexOutOfBoundsException();
        }
        if (x == 0 || x == getWidth() - 1 || y == 0 || y == getHeight() - 1) {
            return Math.pow(255.0, 2) * 3;
        }
        Color x1 = pic.get(x - 1, y);
        Color x2 = pic.get(x + 1, y);
        Color y1 = pic.get(x, y - 1);
        Color y2 = pic.get(x, y + 1);
        return Math.pow((x1.getRed() - x2.getRed()), 2) + Math.pow((x1.getGreen() - x2.getGreen()), 2) + Math.pow((x1.getBlue() - x2.getBlue()), 2)
                + Math.pow((y1.getRed() - y2.getRed()), 2) + Math.pow((y1.getGreen() - y2.getGreen()), 2) + Math.pow((y1.getBlue() - y2.getBlue()), 2);
    }

    private int[] getSeam(String mode, HashMap edgeTo, String end) throws IllegalArgumentException {
        int size;
        if (mode.equals("h")) {
            size = getWidth();
        } else if (mode.equals("v")) {
            size = getHeight();
        } else {
            throw new IllegalArgumentException();
        }
        int[] path = new int[size];
        String cur = end;
        while (size > 0) {
            path[--size] = str2id(mode, cur);
            cur = (String) edgeTo.get(cur);
        }
        return path;
    }

    private String id2str(int col, int row) {
        return col + " " + row;
    }

    private int str2id(String mode, String str) throws IllegalArgumentException {
        if (mode.equals("v")) {
            return Integer.parseInt(str.split(" ")[0]);
        }
        if (mode.equals("h")) {
            return Integer.parseInt(str.split(" ")[1]);
        }
        throw new IllegalArgumentException();
    }

    public int[] findHorizontalSeam() {
        String mode = "h";
        HashMap<String, String> edgeTo = new HashMap<>();
        HashMap<String, Double> energyTo = new HashMap<>();
        double cost = Double.MAX_VALUE;
        String cur, next, end = null;

        for (int col = 0; col < getWidth() - 1; col++) {
            for (int row = 0; row < getHeight(); row++) {
                cur = id2str(col, row);
                if (col == 0) {
                    edgeTo.put(cur, null);
                    energyTo.put(cur, energy(col, row));
                }
                for (int i = row - 1; i <= row + 1; i++) {
                    if (i >= 0 && i < getHeight()) {
                        next = id2str(col + 1, i);
                        double newEng = energy(col + 1, i) + energyTo.get(cur);
                        if (energyTo.get(next) == null || newEng < energyTo.get(next)) {
                            edgeTo.put(next, cur);
                            energyTo.put(next, newEng);
                            if (col + 1 == getWidth() - 1 && newEng < cost) {
                                cost = newEng;
                                end = next;
                            }
                        }
                    }
                }
            }
        }
        return getSeam(mode, edgeTo, end);
    }

    public int[] findVerticalSeam() {
        String mode = "v";
        HashMap<String, String> edgeTo = new HashMap<>();
        HashMap<String, Double> energyTo = new HashMap<>();
        double cost = Double.MAX_VALUE;
        String cur, next, end = null;

        for (int row = 0; row < getHeight() - 1; row++) {
            for (int col = 0; col < getWidth(); col++) {
                cur = id2str(col, row);
                if (row == 0) {
                    edgeTo.put(cur, null);
                    energyTo.put(cur, energy(col, row));
                }
                for (int k = col - 1; k <= col + 1; k++) {
                    if (k >= 0 && k < getWidth()) {
                        next = id2str(k, row + 1);
                        double newEng = energy(k, row + 1) + energyTo.get(cur);
                        if (energyTo.get(next) == null || newEng < energyTo.get(next)) {
                            edgeTo.put(next, cur);
                            energyTo.put(next, newEng);
                            if (row + 1 == getHeight() - 1 && newEng < cost) {
                                cost = newEng;
                                end = next;
                            }
                        }
                    }
                }
            }
        }
        return getSeam(mode, edgeTo, end);
    }

    private boolean isValidSeam(int[] seam) {
        for (int i = 0; i < seam.length - 1; i++) {
            if (Math.abs(seam[i] - seam[i + 1]) > 1) {
                return false;
            }
        }
        return true;
    }

    public void removeHorizontalSeam(int[] seam) throws IllegalArgumentException {
        if (getWidth() <= 1 || getHeight() <= 1 || seam.length < 0 || seam.length > getWidth() || !isValidSeam(seam)) {
            throw new IllegalArgumentException();
        }
        Picture newPic = new Picture(getWidth(), getHeight() - 1);
        for (int col = 0; col < getWidth(); col++) {
            for (int row = 0; row < getHeight() - 1; row++) {
                if (row < seam[col]) {
                    newPic.set(col, row, pic.get(col, row));
                } else {
                    newPic.set(col, row, pic.get(col, row + 1));
                }
            }
        }
        height--;
        pic = new Picture(newPic);
    }

    public void removeVerticalSeam(int[] seam) throws IllegalArgumentException {
        if (getWidth() <= 1 || getHeight() <= 1 || seam.length < 0 || seam.length > getHeight() || !isValidSeam(seam)) {
            throw new IllegalArgumentException();
        }
        Picture newPic = new Picture(getWidth() - 1, getHeight());
        for (int row = 0; row < getHeight(); row++) {
            for (int col = 0; col < getWidth() - 1; col++) {
                if (col < seam[row]) {
                    newPic.set(col, row, pic.get(col, row));
                } else {
                    newPic.set(col, row, pic.get(col + 1, row));
                }
            }
        }
        width--;
        pic = new Picture(newPic);
    }

    public void carve(String axis, int part, int threadIndex, int toDimension, int base, boolean isLast, Function<SeamCarver, int[]> find, BiConsumer<SeamCarver, int[]> remove) {
        if (isLast) {
            for (int i = (threadIndex * part) + toDimension; i < base; i++) {
                System.out.println("resizing... Currently at " + axis + " " + i + " thread: " + (threadIndex + 1));
                int[] seam = find.apply(this);
                remove.accept(this, seam);
            }
        } else {
            for (int i = (threadIndex * part) + toDimension; i < (threadIndex + 1) * part + toDimension; i++) {
                System.out.println("resizing... Currently at " + axis + " " + i + " thread: " + (threadIndex + 1));
                int[] seam = find.apply(this);
                remove.accept(this, seam);
            }
        }
    }

    public Picture resizeTo(String mode, int dimension) throws IllegalArgumentException, InterruptedException {
        if (!mode.equals("width") && !mode.equals("height")) {
            throw new IllegalArgumentException();
        }

        Thread[] threads = new Thread[threadN];
        if (mode.equals("width")) {
            startThreads("width", (this.getWidth() - dimension) / threadN, dimension, this.getWidth(), threads, SeamCarver::findVerticalSeam, SeamCarver::removeVerticalSeam);
        } else {
            startThreads("height", (this.getHeight() - dimension) / threadN, dimension, this.getHeight(), threads, SeamCarver::findHorizontalSeam, SeamCarver::removeHorizontalSeam);
        }

        for (Thread thread : threads) {
            thread.join();
        }

        return this.getPicture();
    }

    private void startThreads(String axis, int part, int dimension, int base, Thread[] threads, Function<SeamCarver, int[]> find, BiConsumer<SeamCarver, int[]> remove) {
        for (int i = 0; i < threads.length; i++) {
            int threadIndex = i;
            boolean isLast = i == threads.length - 1;
            threads[i] = new Thread(() -> {
                carve(axis, part, threadIndex, dimension, base, isLast, find, remove);
            });
            threads[i].start();
        }
    }
}
